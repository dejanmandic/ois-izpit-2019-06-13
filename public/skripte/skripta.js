/* global L, $ */

// seznam z markerji na mapi
var markerji = [];

var mapa;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 9,
    maxZoom: 12
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Akcija ob kliku na gumb za prikaz zgodovine
  $("#prikaziZgodovino").click(function() {
    window.location.href = "/zgodovina";
  });

  function obKlikuNaMapo(e) {
    // Izbriši prejšnje oznake
    for (var i = 0; i < markerji.length; i++) {
      mapa.removeLayer(markerji[i]);
    }

    var koordinate = e.latlng;
    
    var serverUrl = "https://api.lavbic.net/kraji/lokacija?lat=" + koordinate.lat + "&lng=" + koordinate.lng;
    var apiUrl = "https://api.lavbic.net/kraji/lokacija?lat=" + koordinate.lat + "&lng=" + koordinate.lng;
    $.getJSON(apiUrl,function(data){
      
      //console.log(data);
      var myList = document.getElementById("bliznjiKraji");
      //document.getElementsByTagName('body')[0].appendChild(myList);
      var listElement = document.createElement('ul');
      myList.style.marginRight = 200;
      myList.appendChild(listElement);
      
      for (var i = 0; i < 5; ++i) {
        var listItem = document.createElement('li');
        var listText = "<strong>" + data[i].postnaStevilka + " </strong>  " + data[i].kraj;
        listItem.innerHTML = listText;
        listElement.appendChild(listItem);
      }
      
      var iconGET = "/vreme/" + data[0].postnaStevilka;
      $.get(iconGET, function(data) {
          //console.log(data);
         
          
          
      })
    })

   
      








    // Dodaj trenutno oznako
    dodajMarker(koordinate.lat, koordinate.lng,
      "<b>Izbrana lokacija</b><br>(" + koordinate.lat + ", " + koordinate.lng + ")");
      

    // Prikaži vremenske podatke najbližjega kraja
    $("#vremeOkolica").html("<h3>Vreme v okolici kraja<br>...<br></h3>");

    // Shrani podatke najbližjega kraja
    $.get('/zabelezi-zadnji-kraj/' + JSON.stringify({
      postnaStevilka: 1000,
      kraj: "Ljubljana",
      vreme: "neznano",
      temperatura: 10
    }), function(t) {
      $("#prikaziZgodovino").html("Prikaži zgodovino ... krajev");
    });

    // Prikaži seznam 5 najbližjih krajev
    $("#bliznjiKraji").html("<h3>Bližnji kraji</h3>");
  }

  mapa.on('click', obKlikuNaMapo);

  // Na začetku postavi lokacijo na FRI
  var FRIpoint = new L.LatLng(FRI_LAT, FRI_LNG);
  mapa.fireEvent('click', {
    latlng: FRIpoint,
    layerPoint: mapa.latLngToLayerPoint(FRIpoint),
    containerPoint: mapa.latLngToContainerPoint(FRIpoint)
  });
});


/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah
 *
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 */
function dodajMarker(lat, lng, opis) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-icon-2x-blue.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  var marker = L.marker([lat, lng], {icon: ikona});
  marker.bindPopup("<div>" + opis + "</div>").openPopup();
  marker.addTo(mapa);
  markerji.push(marker);
}


